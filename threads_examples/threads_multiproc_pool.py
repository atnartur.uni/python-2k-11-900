import multiprocessing


def func(number):
    print(number)
    return number


if __name__ == '__main__':
    with multiprocessing.Pool() as pool:
        lst = list(range(10000000))
        res = pool.map(func, lst)
