"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from feedback_forms.views import main_page_view, hello_view, answer_page_view, auth_view, create_view, logout_view

urlpatterns = [
    path('', main_page_view, name='main'),
    path('auth/', auth_view, name='auth'),
    path('log_out/', logout_view, name='logout'),
    path('create/', create_view, name='create'),
    path('<str:name>/', hello_view),
    path('answer/<int:pk>/', answer_page_view, name='answer')
]
