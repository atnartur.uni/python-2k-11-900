from unittest import mock

from tests_examples.cat import get_cats_count


def test__check_cats_count():
    with mock.patch(
        'tests_examples.cat.get_cats',
        side_effect=lambda: [{'cat for': 'test'}]
    ):
        count = get_cats_count()
        assert count > 0
