from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

from feedback_forms.forms import FeedbackForm, AuthForm
from feedback_forms.models import FormAnswer


def main_page_view(request):
    answers = FormAnswer.objects.all().order_by('-date')[:10]

    response = render(request, 'feedback_forms/main.html', {
        'answers': answers,
        'user': request.user
        # 'url': request.build_absolute_uri('/our_awesome_url')
    })
    response.set_cookie('my_test_cookie', '1234')
    return response


@login_required
def create_view(request):
    print('cookie:', request.COOKIES.get('my_test_cookie', None))
    form = FeedbackForm()
    message = None
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            answer = FormAnswer(
                name=form.cleaned_data['name'],
                text=form.cleaned_data['text'],
                user=request.user
            )
            answer.save()
            message = 'Ваш ответ успешно сохранен'
    return render(request, 'feedback_forms/create.html', {
        'form': form,
        'message': message,
    })


def hello_view(request, name):
    return render(request, 'feedback_forms/hello.html', {
        'name': name,
        'injection': '<script>alert(1)</script>'
    })


def answer_page_view(request, pk):
    answer = get_object_or_404(FormAnswer, id=pk)
    return render(request, 'feedback_forms/answer.html', {
        'answer': answer
    })


def auth_view(request):
    form = None

    if request.method == 'POST':
        form = AuthForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
            if user is None:
                form.add_error('username', 'Неправильный логин или пароль')
            else:
                login(request, user)
                return redirect('main')

    return render(request, 'feedback_forms/auth.html', {
        'form': form
    })


def logout_view(request):
    logout(request)
    return redirect('main')
