import os
from pathlib import Path

current_path = os.path.abspath(os.curdir)

# ТАК НЕЛЬЗЯ:
# with open(current_path + '\\' + 'test.txt', 'w') as file:
#     file.write('123')


# path.join
# fullpath = os.path.join(current_path, 'test.txt')
# with open(fullpath, 'w') as file:
#     file.write('123')


# pathlib
# fullpath = Path(current_path) / 'test.txt'
# with open(fullpath, 'w') as file:
#     file.write('1234')


# pathlib - open
fullpath = Path(current_path) / 'test.txt'
with fullpath.open('w') as file:
    file.write('12345')
