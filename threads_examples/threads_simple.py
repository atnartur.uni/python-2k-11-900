import random
import threading

def generate_numbers(thread_name):
    for i in range(10):
        print(thread_name, random.randrange(0, 100))

thread1 = threading.Thread(
    target=generate_numbers,
    args=('first',)
)
thread2 = threading.Thread(
    target=generate_numbers,
    args=('second',)
)

thread1.start()
thread2.start()
