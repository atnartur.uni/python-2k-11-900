from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()

class FormAnswer(models.Model):
    name = models.CharField(max_length=200, verbose_name='Имя')
    text = models.TextField(verbose_name='Текст')
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата отправки формы')
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'{self.name} в {self.date}'

    class Meta:
        verbose_name = 'ответ'
        verbose_name_plural = 'ответы'
