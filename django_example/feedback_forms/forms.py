from django import forms


class FeedbackForm(forms.Form):
    name = forms.CharField(label='Имя', min_length=3)
    text = forms.CharField(label='Текст', widget=forms.Textarea)


class AuthForm(forms.Form):
    username = forms.CharField(label='Логин')
    password = forms.CharField(label='Пароль')
