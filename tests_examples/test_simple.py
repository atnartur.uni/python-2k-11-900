import pytest

from tests_examples.cat import Cat, Toy
from tests_examples.utils import div


@pytest.fixture
def cat():
    cat = Cat('Барсик')
    return cat


def test__eat(cat):
    assert cat.fullness == 0
    cat.eat()
    assert cat.fullness == 10


def test__toy_selection(cat):
    toy = cat.select_a_toy()
    assert isinstance(toy, Toy)


@pytest.mark.parametrize("num1,num2,result", [
    (4, 2, 2), (12, 6, 2)
])
def test__div(num1, num2, result):
    assert div(num1, num2) == result


def test__div_0():
    with pytest.raises(ZeroDivisionError):
        div(2, 0)
