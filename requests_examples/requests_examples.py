import random
from pprint import pprint
from xml.etree.ElementTree import ElementTree

import requests
import bs4
from lxml import etree

# response = requests.get('https://http.cat/asdfasdf')
# response.raise_for_status()
# print(response.text)
# pprint(dict(response.headers))
# with open('cat.png', 'wb') as file:
#     # for content in response.iter_content():
#     #     file.write(content)
    #

# POST (create)
# PUT (update)
# DELETE

# Create Read Update Delete

# response = requests.post('https://postman-echo.com/post', data={
#     "a": 1,
#     "b": 2,
#     "my_file": (open('requests_examples.py', 'rb'), 'plain/text')
# })
# print(response.status_code)
# pprint(response.json())


# api_response = requests.get('http://cat-fact.herokuapp.com/facts',)
# api_response.raise_for_status()
# response_data = api_response.json()
# facts = response_data['all']
# pprint(dict(api_response.headers), width=1)
# pprint(facts, width=1)
# random_index = random.randint(0, len(facts) - 1)
# pprint(facts[random_index])

# Задача: написать консольную программу,
# которая принимает IP и возвращает страну
# (https://ip-api.com/docs/api:json).
# Если IP не существует, нужно вывести ошибку
# "Такого IP не существует".

# headers = {'Host': 'yandex.ru'}
# response = requests.get('http://77.88.55.77', headers=headers)
# response.raise_for_status()
# print(response.status_code)
# print(response.url)
# pprint(dict(response.headers))


#id
from bs4 import Tag

response = requests.get('https://habr.com')
response.raise_for_status()
html = response.text
tree = bs4.BeautifulSoup(html, 'html.parser')

# обращение по ID
# scroll_to_top_link = tree.select('#scroll_to_top')[0]
# print(scroll_to_top_link)  # получение HTML
# print(scroll_to_top_link.text)  # получение текста
# print(type(tree))

# запрос по классу
# page_header_element = tree.select('.page-header')[0]
# print(page_header_element)
# print(page_header_element.text.strip())

# получение заголовка первой статьи
# page_header_element = tree.select('.post__title_link')[0]
# print(page_header_element.text.strip())
#
# for tag_element in tree.select('h2'):
#     tag_a: Tag = tag_element.select_one('a')
#     if tag_a is None:
#         continue
#
#     if 'href' not in tag_a.attrs:
#         continue
#
#     href = tag_a.attrs['href']
#     print(href)

### Задача: Написать консольную программу,
# которая принимает адрес страницы и скачивает
# все картинки с этой страницы в папку

# image content type
# response = requests.get('https://http.cat/200')
# content_type = response.headers['Content-Type']
# image_format = content_type.split('/')[-1]
# filename = f'image.{image_format}'
# print(image_format)


response = requests.get('https://habr.com/ru/rss/all/all/?fl=ru')

root: ElementTree = etree.XML(response.text.encode())
channel = root.find('channel')
for item in channel.iterfind('item'):
    title = item.find('title').text
    guid = item.find('guid')
    print(guid.attrib['isPermaLink'], guid.text)
    print(title)


### Задача: написать парсер фида http://tass.ru/rss/v2.xml
# с выводом названий, даты в формате ДД.ММ.ГГГГ и
