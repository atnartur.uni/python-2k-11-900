def div(x, y):
    """
    Деление

    >>> div(4, 2)
    2.0

    :param x:
    :param y:
    :return:
    """
    return x / y


if __name__ == '__main__':
    import doctest
    doctest.testmod()
