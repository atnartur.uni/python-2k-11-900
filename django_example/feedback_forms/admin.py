from django.contrib import admin

from feedback_forms.models import FormAnswer


class FormAnswerAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)


admin.site.register(FormAnswer, FormAnswerAdmin)
