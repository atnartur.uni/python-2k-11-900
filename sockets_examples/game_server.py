import socket
import time

from utils import get_socket_server_ip

host = ''
port = 1235
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print('server ip:', get_socket_server_ip())

s.bind((host, port))
s.listen(3)

connections = []

print('Алло')
try:
    while True:
        conn, addr = s.accept()
        print('connect', conn, addr)
        conn.send(b'hello')
        connections.append(conn)
        time.sleep(1)

        for conn in connections:
            try:
                conn.send(b'asdf')
            except BrokenPipeError:
                print('disconnected', conn)
except BaseException as e:
    print(e)
    s.close()
