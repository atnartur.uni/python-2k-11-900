import random
from collections import namedtuple

import requests

Toy = namedtuple('Toy', [
    'name', 'is_for_cat'
])

TOYS = [
    Toy('slippers', True),
    Toy('piece_of_paper', True),
    Toy('toy_mouse', True),
    Toy('phone', False),
]


class Cat:
    def __init__(self, name):
        self.name = name
        self.fullness = 0

    def eat(self):
        self.fullness += 10

    def select_a_toy(self):
        cat_toys = list(filter(lambda x: x[1], TOYS))
        return random.choice(cat_toys)


def get_cats():
    response = requests.get('http://cat-fact.herokuapp.com/facts')
    return response.json()['all']


def get_cats_count():
    cats = get_cats()
    return len(cats)
