import random
import multiprocessing
import time


def generate_numbers(thread_name, lst):
    for i in range(10):
        number = random.randrange(0, 100)
        time.sleep(0)
        # lst.append(number)
        lst[i] = number
    print(thread_name, lst)


if __name__ == '__main__':
    lst = multiprocessing.Array('i', 10)
    process1 = multiprocessing.Process(
        target=generate_numbers,
        args=('first', lst)
    )
    process2 = multiprocessing.Process(
        target=generate_numbers,
        args=('second', lst)
    )

    process1.start()
    process2.start()

    process1.join()
    process2.join()

    print('final')
    for item in lst:
        print(item)
