first_name = "Eric"
last_name = "Idle"
age = 74
profession = "comedian"
affiliation = "Monty Python"


print("Hello, " + first_name + " " + last_name + ". " + "You are " + str(age) + ". " + "You are a " + profession + ". " + "You were a member of " + affiliation)

print("Hello, %s %s. You are %s. You are a %s. You were a member of %s." % (first_name, last_name, age, profession, affiliation))

print("Hello, {first_name} {last_name}. You are {age}. You are a {profession}. You were a member of {affiliation}.".format(first_name=first_name, last_name=last_name, age=age, profession=profession, affiliation=affiliation))

print(f"Hello, {first_name} {last_name}. You are {age}. You are a {profession}. You were a member of {affiliation}.")
