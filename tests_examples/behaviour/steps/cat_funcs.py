from behave import *

from tests_examples.cat import Cat


@given('cat')
def create_cat(context):
    context.cat = Cat('Барсик')
    context.fullness = context.cat.fullness


@when('eat')
def eating(context):
    context.cat.eat()


@then('fullness has increased to {number}')
def check_fullness(context, number):
    assert context.cat.fullness == int(number)
