import socket

host = ''
port = 1234

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))

data = s.recv(5)
print('Прием:', data)

s.sendall(b'HI!')
s.close()
