from django.apps import AppConfig


class FeedbackFormsConfig(AppConfig):
    name = 'feedback_forms'
